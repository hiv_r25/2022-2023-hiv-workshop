# Workshop Parts
- [Data Science Workshops](data_science/data_science_TOC.md)
- Statistics Workshops
- [Assay Workshops](assays/assay_TOC.md)

# Computing Environments
- [DCC OnDemand URL](https://dcc-ondemand-01.oit.duke.edu/)
- [Getting started with DCC OnDemand](misc/ondemand_howto.md)

# Workshop Content
- [Initial download of workshop content](misc/git_cloning.md)
- [Update workshop content](misc/git_pull.md)

**This website can be reached at: https://bit.ly/3BY1nZd**

![QR Code for this Gitlab Repo](misc/images/qrcode_hiv2022_23_gitlab.png){width=30% height=30%}
