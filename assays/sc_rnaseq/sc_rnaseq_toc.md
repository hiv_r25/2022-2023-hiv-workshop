## scRNA-Seq
1. [Background Slides](slides/scrna_background.pdf)
2. [Bioinformatic Analysis: PBMC 1k](notebooks/cellranger_count_pbmc1k.Rmd)
3. [Statistical Analysis Slides](https://gitlab.oit.duke.edu/hiv_r25/2022-2023-hiv-workshop/-/blob/main/assays/sc_rnaseq/slides/scRNAseq_Analysis_Primer_2023-04-05.pdf)
4. [Statistical Analysis Notebook](notebooks/scRNA-seq_tutorial-seurat-version.Rmd)
5. [Statistical Analysis HTML](notebooks/scRNA-seq_tutorial-seurat-version.html)

## Appendix
1. [Configuration File](notebooks/config.R)
2. [Download Data](notebooks/setup/download_10x_data.Rmd)
