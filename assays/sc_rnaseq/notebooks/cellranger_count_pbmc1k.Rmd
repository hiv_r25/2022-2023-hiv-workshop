---
title: Run Cellranger Count on 1K PBMCs
output: html_document
---

https://support.10xgenomics.com/single-cell-gene-expression/software/pipelines/latest/using/tutorial_ct

# Set Up

```{r}
source("config.R")
```

# Run Cellranger
```{bash}
cellranger count --help
```

```{bash}
set -u

CURDIR=$PWD
cd $OUT_DIR

cellranger count \
    --id=run_count_1kpbmcs \
    --fastqs=${PBMC1K_FASTQ_DIR} \
    --sample='pbmc_1k_protein_v3_gex' \
    --localcores=12 \
    --localmem=60 \
    --transcriptome=${GENOME_DIR} > pbmc_1k_v3_stdout.txt

cd $CURDIR
```

# Explore Output
```{bash}
ls $OUT_DIR
```

```{bash}
cat $OUT_DIR/pbmc_1k_v3_stdout.txt
```

```{bash}
ls $OUT_DIR/run_count_1kpbmcs/outs
```

```{bash}
echo $OUT_DIR/run_count_1kpbmcs/outs
```

