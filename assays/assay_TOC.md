
### Introduction to High-throughput Sequencing (3/2/2023)
  - [High-Throughput Sequencing Lecture](assays/rnaseq_bioinformatics/hts_background.pdf)
  
### Bioinformatics for Bulk RNA-seq (3/9/2023)
  - [Bulk RNA-Seq Bioinformatics Table of Contents](rnaseq_bioinformatics/bulk_rnaseq_TOC.md)

### Statistical Analysis for RNA-seq  (3/16/2023)

### Microbiome Analysis (3/23/2023)
  - [Microbiome Table of Contents](microbiome/microbiome_toc.md)

### Bioinformatics for Flow Cytometry  (3/30/2023)

### Bioinformatics for scRNA-seq (4/6/2023)
