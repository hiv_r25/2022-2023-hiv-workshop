---
title: Quality Control of Raw Data
output: html_document
---

# Setup

## Load Libraries
Assign the variables in this notebook.

```{r}
library(fs)
```

## Shell Variables
### Define R paths
```{r}
fastq_dir="/hpc/group/hiv-r25-2022/data/rnaseq/sra_fastqs"
scratch_dir="/work"

username=Sys.info()[["user"]]
out_dir=file.path(scratch_dir,username,"hiv2023","rnaseq")
qc_dir=file.path(out_dir,"qc_dir")

num_cpus=16
```

### Make new directories
The directories *do* carry over between notebooks, they are a function of the server, so we only need to make the directories that are new in this notebook
```{r}
dir_create(qc_dir)
```



Now let's check to be sure that worked.  We will run `ls` and check that these directories now exist in the `$OUTPUT` directory.
```{r}
file.info(qc_dir)
```
### Define Shell paths
```{r}
Sys.setenv(RAW_FASTQS=fastq_dir)
Sys.setenv(QC_DIR=qc_dir)
Sys.setenv(NUM_CPUS=num_cpus)
```


## Fastqc
Now that we have had a quick look at our fastq file, let's run some basic analysis using a program called fastqc.  This will take less than a minute to run.  Remember that while "long" jobs are running it will say "In [\*]:" in the left margin, once it is done, a number will replace the asterisk.

```{bash}
fastqc -h
```

```{bash}
set -u
fastqc --threads $NUM_CPUS --extract $RAW_FASTQS/SRR12933600.fastq.gz -o $QC_DIR
```

Once fastqc is done running we can view the results by finding the output in the RStudio *Files* pane, it should be in:

```{r}
print(qc_dir)
```

## MultiQC
FastQC is a useful tool, but it has one problem: it generates one report for each FASTQ file.  When you have more than a handful of FASTQs (as most projects will), it is tedious to look at each one, and there is no simple way to compare them.

MultiQC is a solution to this problem.  It mines the results from FastQC (and other HTS analysis tools) and generates reports that combine and summarize results for all the FASTQs analyzed.

### Run FastQC on Multiple FASTQs
Let's pick a selection of FASTQs to look at - we will run FastQC on FASTQs SRR12933740 through SRR12933749 (because it is easy to specify these files with a simple command).

```{bash}
ls $RAW_FASTQS/SRR1293360?.fastq.gz
```

`--threads $NUM_CPUS` tells fastqc to run using $NUM_CPUS cores (defined above as 16).

```{bash}
fastqc --quiet --threads $NUM_CPUS --extract $RAW_FASTQS/SRR1293360?.fastq.gz -o $QC_DIR
```

### Run MultiQC

```{bash}
multiqc -h
```

```{bash}
multiqc $QC_DIR --outdir $QC_DIR
```

Once multiqc is done running we can view the results by finding the output in the RStudio *Files* pane, it should be in a file named `multiqc_report.html` in :
```{r}
print(qc_dir)
```

