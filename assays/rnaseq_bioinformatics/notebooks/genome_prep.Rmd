---
title: Download and Index Genome
output: html_document
---

# Setup
## Load Libraries
```{r}
library(fs)
library(here)
library(dplyr)
library(R.utils)
```

## Assign the variables in this notebook.

```{r}
"assays/rnaseq_bioinformatics/notebooks/config.R" %>%
    here() %>%
    source
```

## Making New Directories
Make the directories that are new in this notebook
```{r}
dir_create(genome_dir)
dir_create(star_dir)
```


Now let's check to be sure that worked.  
```{r}
file_info(c(genome_dir, star_dir))
```



## STAR
* Publication: https://doi.org/10.1093/bioinformatics/bts635
* Github repo: https://github.com/alexdobin/STAR
* Manual: https://github.com/alexdobin/STAR/blob/master/doc/STARmanual.pdf


## Reference Genome and Annotation
We have some preparation to do before we can map our data.  First we need to download a reference genome and its annotation file.  It is very important that **the genome sequence and annotation are the same version**, if they are not, things could go horribly wrong.  The best way to ensure that your sequence and annotation are compatible is to download both from the same place, at the same time, and double check that they have the same version number.  There are several good places to get genomes data:

* Illumina's website http://support.illumina.com/sequencing/sequencing_software/igenome.html.
* NCBI http://www.ncbi.nlm.nih.gov
* Ensembl http://www.ensembl.org/info/about/species.html

Illumina does not have any *Cryptococcus neoformans* genomes, so we need to look elsewhere.

### NCBI
NCBI has most published genomes, but it is a bit tricky to find exactly what we are looking for.  A good place to start is the [NCBI Genome Assembly page](http://www.ncbi.nlm.nih.gov/assembly/) where we can search for "Cryptococcus neoformans H99".

But the mapping software that we will be using, STAR, does not like the GFF format that NCBI uses for annotation.  We could get the GFF from NCBI and convert it to a format that STAR likes, but it is easier to look elsewhere to see if we can find a GTF formatted file that STAR likes.

### Ensembl
Our next stop is Ensembl, which is also difficult to navigate.  
1. Start at the [Ensembl Fungi](http://fungi.ensembl.org/index.html) page
2. Click on [View full list of all Ensembl Fungi species](http://fungi.ensembl.org/species.html)
3. Find "Cryptococcus neoformans var. grubii H99" (it is helpful to type "H99" in the filter box)
4. There is a link to the [different FASTA files](ftp://ftp.ensemblgenomes.org/pub/fungi/release-39/fasta/fungi_basidiomycota1_collection/cryptococcus_neoformans_var_grubii_h99), we want :
    ftp://ftp.ensemblgenomes.org/pub/release-39/fungi/fasta/fungi_basidiomycota1_collection/cryptococcus_neoformans_var_grubii_h99/dna/Cryptococcus_neoformans_var_grubii_h99.CNA3.dna.toplevel.fa.gz
5. There is also a link to GFF annotation, but we want GTF and the trail has gone cold. h ere is no obvious sign that there is a GTF file, you just have to know where to find it:
ftp://ftp.ensemblgenomes.org/pub/release-39/fungi/gtf/fungi_basidiomycota1_collection/cryptococcus_neoformans_var_grubii_h99/Cryptococcus_neoformans_var_grubii_h99.CNA3.39.gtf.gz

### Downloading with download.file
Now we can use the `download.file` command to actually download these files. We will get the files from NCBI. Here is what we will want to tell wget:

* --directory-prefix : the directory to save the files in
* --no-verbose : don't output a lot of information while downloading
* URL : what to download

We are going to make a "genome" directory for these files so that things don't get too messy.  Soon we will generate several files based on these that tophat needs.  I generally like to keep the original file names, but we are changing the names to make typing easier later.

```{r}
download.file(url=gtf_url, 
              destfile = gtf_gz_path)

download.file(url=fasta_url, 
              destfile = fasta_gz_path)
list.files(genome_dir)
```


### Decompressing the reference files
Next we need to uncompress the files using `gunzip`

```{r}
gunzip(gtf_gz_path)
gunzip(fasta_gz_path)
```

And now we have uncompressed versions of the files
```{r}
list.files(genome_dir, full.names=TRUE)
```

Let's take a quick look at these files.  The `head` command shows us the first few lines of a file (default is 10).

```{bash}
head $FASTA
```

```{bash}
head $GTF
```

Unfortunately the GFF file has long lines, which are wrapping onto the next line, making them hard to read.  Another option is to use the command `less -S ${GENOME_DIR}/Cryptococcus_neoformans_var_grubii_h99.CNA3.39.gtf` in the terminal (after sourcing the configuration script).  The "-S" tells `less` to truncate lines instead of wrapping them.

> #### GTF, a brief aside
> You can find one description of the GTF format [here](https://useast.ensembl.org/info/website/upload/gff.html).  Unfortunately it is not entirely standard.

### Indexing the Genome
Before we can map reads to the reference genome using STAR, we need to index it. This will generate a transformed version of the genome that allows STAR to efficiently map sequences to it. We run STAR in "genomeGenerate" mode to do this. 

So here is how we run STAR for genome indexing: 

```{bash}
set -u
STAR \
    --runMode genomeGenerate \
    --genomeDir $GENOME_DIR \
    --genomeFastaFiles ${FASTA} \
    --sjdbGTFfile ${GTF} \
    --outFileNamePrefix ${STAR_DIR}/genome_ \
    --genomeSAindexNbases 11 \
    --runThreadN $NUM_CPUS
```

STAR has *a lot* of command line options!  So here is what the above command is doing:
* --runMode genomeGenerate: index the genome
* --genomeDir : output genome index files to this directory
* --genomeFastaFiles : genome sequence file (in FASTA format)
* --sjdbGTFfile : annotation file (in GTF format)
* --outFileNamePrefix : prefix all output files with this string
* --genomeSAindexNbases : selects string length for index, needs to be adjusted based on genome size, can also be made smaller to reduce memory usage at cost of speed
* --runThreadN : tells STAR to run using multiple cores.  I am using it so we don't have to wait too long for this to run during class.  It is OK to use multiple cores, but before you do this you should be sure that the server is not busy, and even then you should use a reasonable number of cores.  Abusing multi-threading is inconsiderate of other users and could crash the server.

Now let's check to be sure that worked:

```{r}
list.files(genome_dir, full.names=TRUE)
```
