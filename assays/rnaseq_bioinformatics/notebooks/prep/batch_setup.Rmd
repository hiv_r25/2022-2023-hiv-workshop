---
title: "Untitled"
output: html_document
date: "2023-03-07"
---

This is run by sbatch_run_fastqs_to_counts.sh to set up directories and index genome

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
library(dplyr)
library(here)
```

```{r}
here("assays/rnaseq_bioinformatics/notebooks/config.R") %>%
  source()

dir_create(out_dir)
dir_create(qc_dir)
dir_create(trim_dir)
dir_create(star_dir)
```


# Run Genome Prep
Download and index genome if genome index is missing
```{r}
genome_prep_rmd = here("assays/rnaseq_bioinformatics/notebooks/genome_prep.Rmd")

if (file_exists(file.path(genome_dir,"SA"))){
  print("Genome Index looks good")
} else {
  print("MISSING Genome Index. Building it now")
  render(genome_prep_rmd,
       output_format="html_document",
       output_dir=out_dir)
}
```
