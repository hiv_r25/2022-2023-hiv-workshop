#!/usr/bin/env bash
set -u

# This is run by sbatch_run_fastqs_to_counts.sh through an sbatch array

singularity exec $BIND_ARGS $SINGULARITY_IMAGE Rscript -e "rmarkdown::render('$HOME/project_repos/teaching/2022-2023-hiv-workshop/assays/rnaseq_bioinformatics/notebooks/prep/batch_run.Rmd')"
