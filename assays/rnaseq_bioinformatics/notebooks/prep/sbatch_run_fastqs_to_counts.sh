#!/bin/bash

# This script will trim and count all the FASTQs in the data directory
# It uses sbatch array, so it is fast (total run time of about 2 minutes)

export SINGULARITY_IMAGE="/opt/apps/community/od_chsi_rstudio/hiv_r25_rstudio.sif"
export BIND_ARGS="--bind /work:/work --bind /hpc/group:/hpc/group"

export BATCH_THREADS=8
export BATCH_TOT_MEM=10G

RAW_FASTQS="/hpc/group/hiv-r25-2022/data/rnaseq_polya/sra_fastqs"

FASTQ_R1_FILES=($(ls -1tr ${RAW_FASTQS}/*.fastq.gz))


srun -A chsi -p chsi singularity exec $BIND_ARGS $SINGULARITY_IMAGE Rscript -e "rmarkdown::render('$HOME/project_repos/teaching/2022-2023-hiv-workshop/assays/rnaseq_bioinformatics/notebooks/prep/batch_setup.Rmd')"

echo "Total Jobs: ${#FASTQ_R1_FILES[@]}"
sbatch -W --array=1-${#FASTQ_R1_FILES[@]} -A chsi -p chsi --mem=${BATCH_TOT_MEM} -c ${BATCH_THREADS} $HOME/project_repos/teaching/2022-2023-hiv-workshop/assays/rnaseq_bioinformatics/notebooks/prep/batch_subscript.sh
