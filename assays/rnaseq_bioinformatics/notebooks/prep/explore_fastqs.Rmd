---
title: "Untitled"
output: html_document
date: "2023-03-07"
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```



```{r}
library(tibble)
library(stringr)
library(dplyr)
library(readr)
library(here)
```

```{r}
here("assays/rnaseq_bioinformatics/notebooks/config.R") %>%
  source()
```

```{r}
accessions_file %>%
  read_csv ->
  accessions_with_meta



list.files(fastq_dir, full.names = TRUE)%>%
  file_info %>%
  arrange(size) %>%
  select(path, size) %>%
  mutate(sra_id=path %>%
           basename %>%
           str_remove(".fastq.gz")) %>%
  left_join(accessions_with_meta, by=c("sra_id"="Run")) ->
  meta_with_sizes

meta_with_sizes
```

# Poly(A) Enriched (mRNA only)
```{r}
accessions_with_meta %>%
  filter(enrichment_method=="Poly(A)",
         sequencing_batch=="2018")
```

# Ribo_zero Enriched (Total RNA)
```{r}
accessions_with_meta %>%
  filter(enrichment_method=="Ribo-Zero",
         sequencing_batch=="2018")
```
